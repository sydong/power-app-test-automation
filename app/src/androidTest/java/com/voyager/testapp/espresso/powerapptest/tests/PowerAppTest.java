package com.voyager.testapp.espresso.powerapptest.tests;

import android.os.Environment;
import android.os.RemoteException;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;
import android.support.test.uiautomator.UiDevice;
import android.support.test.uiautomator.UiObject;
import android.support.test.uiautomator.UiObjectNotFoundException;
import android.support.test.uiautomator.UiSelector;
import android.test.ActivityInstrumentationTestCase2;
import android.util.Log;

import com.voyager.testapp.espresso.powerapptest.MainActivity;
import com.voyager.testapp.espresso.powerapptest.model.PromoCode;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import au.com.bytecode.opencsv.CSVReader;

/**
 * Created by voyager on 11/20/15.
 */

@RunWith(AndroidJUnit4.class)
public class PowerAppTest extends ActivityInstrumentationTestCase2<MainActivity> {

    private UiDevice mDevice;
    private ArrayList<PromoCode> keywords;

    private boolean passed;

    public PowerAppTest() {
        super(MainActivity.class);
    }

    @Before
    public void setUp() throws IOException, UiObjectNotFoundException, RemoteException {
        injectInstrumentation(InstrumentationRegistry.getInstrumentation());
        mDevice = UiDevice.getInstance(InstrumentationRegistry.getInstrumentation());
        closeAllApps();
        mDevice.pressHome();
        keywords = getPromoCodesFromCsvFile();
    }

    @Test
    public void sendPromoSMS() throws UiObjectNotFoundException, RemoteException, InterruptedException {

        passed = true;

        UiObject SMSIcon =
                mDevice.findObject(new UiSelector().className("android.widget.TextView")
                        .descriptionContains("Messages"));

        SMSIcon.clickAndWaitForNewWindow();

        UiObject composeMessage =
                mDevice.findObject(new UiSelector().className("android.widget.ImageButton")
                        .description("Compose"));

        composeMessage.clickAndWaitForNewWindow();

        sendSMS();

        UiObject listView = mDevice.findObject(new UiSelector().resourceId("android:id/list"));
        UiObject listViewItem = listView.getChild(new UiSelector().index(0));
        UiObject unreadCountIndicator = listViewItem.getChild(new UiSelector().resourceId("com.android.mms:id/unread_count"));

        if (checkNewMessageIndicator(unreadCountIndicator)) {
            listViewItem.clickAndWaitForNewWindow();
            checkSuccessMessage();
        } else
            passed = false;

        assertTrue(passed);
    }

    private void sendSMS() throws UiObjectNotFoundException {
        UiObject editTextRecipient = mDevice.findObject(new UiSelector().resourceId("com.android.mms:id/recipients_editor_to"));
        UiObject editTextBottom = mDevice.findObject(new UiSelector().resourceId("com.android.mms:id/edit_text_bottom"));
        UiObject buttonSend = mDevice.findObject(new UiSelector().resourceId("com.android.mms:id/send_button").enabled(true));

        PromoCode promo = keywords.get(2);

        editTextRecipient.setText("214");
        editTextBottom.setText("?1515");

        buttonSend.waitForExists(1000);

        if (buttonSend.isEnabled())
            buttonSend.click();

        mDevice.findObject(new UiSelector()
                .resourceId("com.android.mms:id/actionbar_arrow"))
                .clickAndWaitForNewWindow();
    }

    private boolean checkNewMessageIndicator(UiObject indicator) throws UiObjectNotFoundException {
        indicator.waitForExists(10000);
        return indicator.exists();
    }

    private boolean checkSuccessMessage() throws UiObjectNotFoundException {
        UiObject listViewHistory = mDevice.findObject(new UiSelector().resourceId("com.android.mms:id/history"));
        UiObject recentHistoryItem = listViewHistory.getChild(new UiSelector().index(listViewHistory.getChildCount() - 1));
        UiObject textBody = recentHistoryItem.getChild(new UiSelector().resourceId("com.android.mms:id/body_text_view"));

        Log.i("BODY", textBody.getText());

        return true;
    }

    private void closeAllApps() throws UiObjectNotFoundException, RemoteException {
        mDevice.pressRecentApps();
        UiObject closeAll =
                mDevice.findObject(new UiSelector().className("android.widget.ImageButton")
                        .description("Close all"));

        closeAll.click();
    }

    private ArrayList<PromoCode> getPromoCodesFromCsvFile() throws IOException {
        ArrayList<PromoCode> codes = new ArrayList<>();

        File sdcard = Environment.getExternalStorageDirectory();
        File file = new File(sdcard,"/Download/POWERAPP KEYWORD.csv");

        CSVReader reader = new CSVReader(new FileReader(file));

        List content = reader.readAll();

        String[] row;

        for (Object object : content) {
            row = (String[]) object;

            PromoCode code = new PromoCode(row[0], row[1], row[2]);

            Log.i("CSV Read", row[0] + " # " + row[1] + " # " + row[2]);

            codes.add(code);
        }
        return codes;
    }
}
