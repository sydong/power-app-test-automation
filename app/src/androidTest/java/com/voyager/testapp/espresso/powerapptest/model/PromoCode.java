package com.voyager.testapp.espresso.powerapptest.model;

/**
 * Created by voyager on 11/20/15.
 */
public class PromoCode {

    private String accessCode;
    private String application;
    private String keyword;

    public PromoCode(String accessCode, String application, String keyword) {
        this.accessCode = accessCode;
        this.application = application;
        this.keyword = keyword;
    }

    public String getAccessCode() {
        return accessCode;
    }

    public void setAccessCode(String accessCode) {
        this.accessCode = accessCode;
    }

    public String getApplication() {
        return application;
    }

    public void setApplication(String application) {
        this.application = application;
    }

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }
}
